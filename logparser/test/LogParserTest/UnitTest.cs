using System;
using Xunit;
using System.IO;
using LogParser;

namespace LogParserTest
{
    public class UnitTests
    {
        /*
                LogParser.exe --log-level Debug --log-dir logs --csv output
                LogParser.exe --how 
                LogParser.exe --how --log-level Debug --log-dir logs --csv output
                LogParser.exe --log-level Debug  --log-level info --log-dir logs --csv output
                LogParser.exe --log-dir logs --csv output
                LogParser.exe --log-level Debug --csv output 
                LogParser.exe --csv output 
                LogParser.exe --log-level Debug --log-dir logs
                LogParser.exe --log-level 
                LogParser.exe --log-level --log-dir logs --csv output
                LogParser.exe --log-level Mera --log-dir logs --csv output
                LogParser.exe --log-dir Wrong --csv output
                LogParser.exe --log-dir pqr --csv Wrong
                LogParser.exe --log-dir pqr --csv Wrong
                LogParser.exe 
                LogParser.exe --log-dir pqr --csv Wrong
                LogParser.exe --log-level Debug --log-dir Wrong --csv output
                LogParser.exe --log-level Debug --log-dir abcd --csv Wrong
         */

        [Fact]
        public void TestHelp()
        {
            string[] input = { "--how" };
            LogParserInput p = new LogParserInput(input);
            CommandLineParams args = p.ParseCommandLine();
            Assert.True(args.NeedHelp);
            Assert.Null(args.Err);
        }
        [Fact]
        public void TestNoCommand()
        {
            string[] input = { };
            LogParserInput p = new LogParserInput(input);
            CommandLineParams args = p.ParseCommandLine();
            Assert.True(args.NeedHelp);
            Assert.Null(args.Err);
        }

        [Fact]
        public void TestIncorrect()
        {
            string[] input = { "--wrong" };
            LogParserInput p = new LogParserInput(input);
            CommandLineParams args = p.ParseCommandLine();
            Assert.True(args.NeedHelp);
            Assert.Equal("Unknown command --wrong", args.Err);
        }

        [Fact]
        public void TestNoLogLevel()
        {
            string[] input = { "--log-level" };
            LogParserInput p = new LogParserInput(input);
            CommandLineParams args = p.ParseCommandLine();
            Assert.True(args.NeedHelp);
            Assert.Equal("Invalid input for --log-level", args.Err);
        }

        [Fact]
        public void TestNoLogDir()
        {
            string[] input = { "--log-dir" };
            LogParserInput p = new LogParserInput(input);
            CommandLineParams args = p.ParseCommandLine();
            Assert.True(args.NeedHelp);
            Assert.Equal("Invalid input for --log-dir", args.Err);
        }

        [Fact]
        public void TestCorrectInput()
        {
            string[] input = { "--log-level", "Debug", "--log-dir", "logs", "--csv", "output" };
            LogParserInput p = new LogParserInput(input);
            CommandLineParams args = p.ParseCommandLine();
            Assert.False(args.NeedHelp);
            Assert.Null(args.Err);
            Assert.Equal("output",args.OutputFilePath);
            Assert.Equal("logs",args.InputPath);
            Assert.True(args.Levels.Contains("Debug"));  // GS -- Assert.Contains("Debug", args.Levels);
        }
 
        [Fact]
        public void TestValidInput()
        {
            string[] input = { "--log-level", "Debug", "--log-dir", @"..\..\..\..\..\logs", "--csv", @".\output.csv" };
            LogParserInput p = new LogParserInput(input);
            CommandLineParams args = p.ParseCommandLine();
            p.ValidateInput(args);
            Assert.False(args.NeedHelp);
            Assert.Null(args.Err);
        }
        [Fact]
        public void TestInvalidLogLevel()
        {
            string[] input = { "--log-level", "NoLog","--log-dir","logs" };
            LogParserInput p = new LogParserInput(input);
            CommandLineParams args = p.ParseCommandLine();
            p.ValidateInput(args);
            Assert.False(args.NeedHelp);
            Assert.Equal("Incorrect Log level. Supported log levels are TRACE, DEBUG, INFO, WARN, ERROR",args.Err);
        }
        [Fact]
        public void TestInvalidLogDir()
        {
            string[] input = { "--log-dir", "Unkonwn-logs" };
            LogParserInput p = new LogParserInput(input);
            CommandLineParams args = p.ParseCommandLine();
            p.ValidateInput(args);
            Assert.False(args.NeedHelp);
            Assert.Equal("Invalid Input Directory Unkonwn-logs",args.Err);
        }
        [Fact]
        public void TestInvalidLogLevels()
        {
            string[] input = { "--log-level", "Debug", "--log-dir", "logs", "--log-level", "NoLogs" };
            LogParserInput p = new LogParserInput(input);
            CommandLineParams args = p.ParseCommandLine();
            p.ValidateInput(args);
            Assert.False(args.NeedHelp);
            Assert.Equal("Incorrect Log level. Supported log levels are TRACE, DEBUG, INFO, WARN, ERROR",args.Err);
        }
        [Fact]
        public void TestInvalidOutputDir()
        {
            string[] input = { "--log-level", "Debug", "--log-dir", @"..\..\..\..\..\logs", "--csv", @"..\InvalidDir\output" };
            LogParserInput p = new LogParserInput(input);
            CommandLineParams args = p.ParseCommandLine();
            p.ValidateInput(args);
            Assert.False(args.NeedHelp);
            Assert.Equal(@"Output path ..\InvalidDir\output does not exists",args.Err);
        }
        [Fact]
        public void TestRequiredArgs()
        {
            string[] input = { "--log-level", "Debug" };
            LogParserInput p = new LogParserInput(input);
            CommandLineParams args = p.ParseCommandLine();
            p.ValidateInput(args);
            Assert.True(args.NeedHelp);
            Assert.Equal("Missing required params",args.Err);
        }
    }
}
