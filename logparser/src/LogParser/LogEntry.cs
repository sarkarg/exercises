
namespace LogParser
{
    public class LogEntry
    {
        public int SrNumber { get; set; }
        public string Level { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Message { get; set; }

        public override string ToString()
        {
            return string.Format("{0,-4}{1,-8}{2,-8}{3,-12}{4}",SrNumber,Level,Date,Time,Message);
            //return $"LogEntry: {SrNumber}|{Level}|{Date}|{Time}|{Message}";
        }

        public string ToCSV() {
            return $"{SrNumber},{Level},{Date},{Time},\"{Message}\"";
        }

    }
}